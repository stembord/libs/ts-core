export { clone, IClone } from "./clone";
export { equals, safeEquals, IEquals } from "./equals";
export {
  Filter,
  ForEach,
  IIterator,
  iter,
  Iterator,
  Map,
  Skip,
  Step,
  ToMap,
  Take
} from "./iter";
export { some, none, Option } from "./option";
export { range, rangeFrom, Range, RangeFrom } from "./range";
export { ok, err, Result } from "./result";
